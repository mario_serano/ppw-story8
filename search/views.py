from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.template.loader import render_to_string

import requests
import json

# Create your views here.
def index(request):
    return render(request, 'search/index.html')

def api(request):
    ctx = {}
    url_param = request.GET.get("q")
    third_party_endpoint = "https://www.googleapis.com/books/v1/volumes?q="

    if (not url_param):
        url_param = "Perancangan & Pemrograman Web"
    request = requests.get(third_party_endpoint + url_param)

    render_html = render_to_string(
        template_name="search/result.html",
        context={
            "request":json.loads(request.text)
        }
    )    
    return JsonResponse({"render_html":render_html}, safe=False)