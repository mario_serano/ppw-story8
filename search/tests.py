from django.test import TestCase, Client
from django.urls import resolve
from . import views

# Create your tests here.
class UnitTestSearch(TestCase):
    def test_story8_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story8_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'search/index.html')
    
    def test_story8_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_story8_api_url_is_exist(self):
        response = Client().get('/api/')
        self.assertEqual(response.status_code, 200)
    
    def test_story8_api_using_api_func(self):
        found = resolve('/api/')
        self.assertEqual(found.func, views.api)